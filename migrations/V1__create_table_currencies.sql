CREATE TABLE currencies(
   currency_id serial PRIMARY KEY,
   name VARCHAR (10) UNIQUE NOT NULL,
   caption VARCHAR (100) NOT NULL
);

